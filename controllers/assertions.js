/**
 * Created by thomasjansen on 08/04/15.
 */
var mongoose = require('mongoose');
var Assertions = mongoose.model('Assertion');
//var runner = require('../controllers/requestController');


exports.createAssertion = function(req, res){
    exports.create = function (req, res){

        var doc = new Assertions(req.body);

        doc.save(function(err){
            var retObj = {
                meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
    }
};

exports.readAssertion = function(req, res){
    var conditions, fields, options;

    conditions = {_id: req.params._id}
        , fields = {}
        , options = {'createdAt': -1};

    Assertions.findOne(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            }
            return res.send(retObj);
        });

};

exports.updateAssertion = function(req, res){
    var conditions =
        {_id: req.params._id}
        , update = {
            name: req.body.name || '',
            expression: req.body.expression || '',
            operator: req.body.operator || '',
            expectation: req.body.expectation || '',
            description: req.body.description || ''
        }
        , options = {multi: false}
        , callback = function (err, doc){
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };

    Assertions.findOneAndUpdate(conditions, update, options, callback);

};

exports.deleteAssertion = function(req, res){
    var conditions, callback, retObj;

    conditions =
    {_id: req.params._id}
        , callback = function (err, doc){
        retObj = {
            meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    };

    Assertions.remove(conditions, callback);

};

exports.readAllAssertions = function(req, res){
    var conditions, fields, options;

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    Assertions.find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};