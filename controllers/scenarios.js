/**
 * Created by thomasjansen on 30/03/15.
 */
var mongoose = require('mongoose');
var Scenario = mongoose.model('Scenario');
var runner = require('../controllers/requestController');


exports.getAll = function(req, res){
    Scenario.find({}, function(err, docs) {
        if (!err){
            console.log(docs);
            return res.send({info:"Get all Scenario",page: "/scenario", data: docs});
        } else {throw err;}
    })
};

exports.createScenario = function(req, res){
    console.log('Create Scenario', req.body);

    var doc = new Scenario(req.body);

    doc.save(
        function(err, doc){
            var retObj = {
                meta : {"action": "create", 'timestamp' : new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            Scenario.find({}, function(err, docs) {
                if (!err){
                    console.log(docs);
                    return res.send({info:"Get all scenarios",page: "/scenarios", data: docs, ret: retObj});
                } else {throw err;}
            })
        }
    )};

exports.getScenario = function(req, res){
    Scenario.find({_id: req.params._id}, function(err, docs) {
        if (!err){
            console.log(docs);
            return res.send({info:"Get test", page: "/scenario/:_id", data: docs});
        } else {throw err;}
    })
};

exports.updateScenario = function(req, res){
    Scenario.findByIdAndUpdate(req.body._id,
        {content : req.body.content},
        function (err, doc) {
            var retObj = {
                meta : {"action": "Update Scenario", 'timestamp' : new Date(), filename: __filename},
                doc : doc,
                err : err
            };
            return res.send({info:"Get all Scenarios", page: "/Scenario/:_id", data: doc, ret: retObj});
        }
    )
};

exports.deleteScenario = function(req, res){
    Scenario.remove({_id: req.params._id},function(err, doc){
        var retObj = {
            meta: {"action": "delete Scenario", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    })
};

exports.executeScenario = function(req, res){
    Scenario.find({_id: req.params._id}, function(err, doc) {
        if (!err){
            console.log(doc);
            return res.send({info:"Get test", page: "/requests", data: runner.internalCall(docs.method, docs.url)});
        } else {throw err;}
    })

};