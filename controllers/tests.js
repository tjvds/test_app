/**
 * Created by thomasjansen on 30/03/15.
 */
var mongoose = require('mongoose');
var Request = mongoose.model('Request');
var runner = require('../controllers/requestController');

exports.getAll = function(req, res){
    Request.find({}, function(err, docs) {
        if (!err){
            return res.send({info:"Get all tests",page: "/requests", data: docs});
        } else {throw err;}
    })
};


exports.createTest = function(req, res){
    console.log('Create request', req.body);
    var doc = new Request(req.body);

    doc.save(
        function(err, doc){
            var retObj = {
                meta : {"action": "create", 'timestamp' : new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            Request.find({}, function(err, docs) {
                if (!err){
                    console.log(docs);
                    return res.send({info:"Get all tests",page: "/requests", data: docs, ret: retObj});
                } else {throw err;}
            })
        }
)};


exports.deleteTest = function(req, res){
    Request.remove({_id: req.params._id},function(err, doc){
        var retObj = {
            meta: {"action": "delete test", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    })
};


exports.updateTest = function(req, res){
        Request.findByIdAndUpdate(req.body._id,
            {content : req.body.content},
            function (err, doc) {
                var retObj = {
                    meta : {"action": "Update Test", 'timestamp' : new Date(), filename: __filename},
                    doc : doc,
                    err : err
                };
                return res.send({info:"Get all tests", page: "/request/:", data: doc, ret: retObj});
            }
        )
};

exports.getTest = function(req, res){
    Request.find({_id: req.params._id}, function(err, docs) {
        if (!err){
            return res.send({info:"Get test", page: "/requests", data: docs});
        } else {throw err;}
    })
};

exports.storeResult = function(req, res){
    Request.findByIdAndUpdate(req.body._id,
        {content : req.body.content},
        function (err, doc) {
            var retObj = {
                meta : {"action": "Update Test", 'timestamp' : new Date(), filename: __filename},
                doc : doc,
                err : err
            };
            return res.send({info:"Get all tests", page: "/request/:", data: doc, ret: retObj});
        }
    )
};

exports.executeTest = function(req, res){

    Request.find({_id: req.params._id}, function(err, doc) {
        if (!err){
            console.log(doc[0]);

            runner.internalRequest(doc[0].method, doc[0].hostname, doc[0].parameters, res, doc[0].assertion);
        } else {
            console.log(err);
            throw err;
        }
    })
};