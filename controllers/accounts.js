/**
 * Created by thomasjansen on 30/03/15.
 */

var passport = require('passport');
var Account = require('../models/account.js');

/*
* Get register page
* */
exports.getRegister = function(req, res){
    res.send({info: "Get the register page", page: "/register"});
};

/*
* Login request
*
* */
exports.registerAccount = function(req, res){
    console.log("register", req.body);
    Account.register(new Account({ username : req.body.username , email : req.body.email}), req.body.password, function(err, account) {
        console.log(account);
        if (err) {
           return res.send({info: "Sorry. That username already exists. Try again.", succes: 0, err: err})
        }

        //console.log("login");
        passport.authenticate('local')(req, res, function () {
           return res.send({info: "Register and login succes", succes: 1});
        });
    });
};


/*
* Get login page
*
* */
exports.getLogin = function(req, res){
    res.send({info: "open login", page: "/login"});
};

/*
 * Login request
 *
 * */
exports.login = function(req, res){
    passport.authenticate('local')(req, res, function() {
        res.send({info: "Login", page: "/", success: 1});
    });

};


/*
 * Logout request
 *
 * */
exports.logout = function(req, res){
    req.logout();
    res.send({info:"logout!", page:'/'});
};

/*
 * Ping
 *
 * */
exports.ping = function(req, res){
    res.send("pong!", 200);
};


exports.userDetails = function(req, res){
    console.log("request parameters : " + req.params._id);
    Account.find({_id: req.params._id}, function(err, docs) {
        if (!err){
            console.log(docs);
            return res.send({info:"Get user", page: "/user/:_id", data: docs});
        } else {throw err;}
    })
};



exports.editUser = function(req, res){
    Account.findOne({_id: req.body._id}, function(err, docs) {
            console.log("User found, updating now.");
            docs.name = req.body.name;
            docs.email = req.body.email;
            docs.password = req.body.password;
            docs.save();

    });
};


exports.users = function(req, res){
    Account.find({}, function(err, docs) {
        if (!err){
            console.log(docs);
            return res.send({info:"Get all accounts", page: "/users", data: docs});
        } else {throw err;}
    })
};

exports.check = function(req,res) {
    if (req.user) {
        console.log(req.user);
        res.send(true);
    }
    else {
        res.send(false);
    }
};


exports.delete = function(req, res){
    Account.remove({_id: req.params._id}, function(err, doc){
        var retObj = {
            meta: {"action": "delete user", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    })

};