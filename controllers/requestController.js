
var http = require('http');
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
var request = require('request');
var _ = require('underscore');
var jsonQuery = require('json-query');

exports.internalRequest = function(method, url, body , res , assertion){
    if (method.toUpperCase() == "POST"){
        postHttp(url, body, assertion, res);
    }else if(method.toUpperCase() == "GET"){
        getHttp(url, body, assertion, res);
    }else if(method.toUpperCase() == "DELETE"){
        deleteHttp(url, body, assertion, res);
    }else if (method.toUpperCase() == "PUT"){
        putHttp(url, body, assertion, res);
    }else {
        res.send("error");
    }
};

function postHttp(url, body, assertion, res){
    var formdata = {};

    if(body != ""){
        for(var i = 0; i < body.length; i++){
            var data = JSON.parse('{"' + body[i].name + '"' + ': "' + body[i].value + '"}');
            formdata = extend({}, formdata, data);
            console.log(formdata);
        }
    };

    request.post({url: url, form: formdata},
        function (err, response, body){
            res.send({res: response, assertion: checkAssertion(response ,assertion), body: body});
    });
}

//function Get
function getHttp(url, body, assertion, res){
    request.get(url, function (err, response, body){
            res.send({res: response, assertion: checkAssertion(response ,assertion), body: body});
    });
}

//function put
function putHttp(url, body, assertion, res) {
    var formdata = {};

    if(body != ""){
        for(var i = 0; i < body.length; i++){
            var data = JSON.parse('{"' + body[i].name + '"' + ': "' + body[i].value + '"}');
            formdata = extend({}, formdata, data);
            console.log(formdata);
        }
    };

    request.put({url: url,
            form: formdata},
        function (err, response, body){
            console.log(err);
            res.send({res: response, assertion: checkAssertion(response ,assertion), body: body});
        });

}

//function delete
function deleteHttp(url, body, assertion, res){
    var formdata = {};

    if(body != ""){
        for(var i = 0; i < body.length; i++){
            var data = JSON.parse('{"' + body[i].name + '"' + ': "' + body[i].value + '"}');
            formdata = extend({}, formdata, data);
            console.log(formdata);
        }
    };
    request.del({url: url,
            form: formdata},
        function (err, response, body){
            checkAssertion(assertion);
            //res.send({res: response, assertion: , body: body});
        });
}


function checkAssertion(response, assertion){
    var success = false;
    var expression;
    var operator;
    var expectation;

    var assertionToTest = assertion[0];

   if (response != undefined && (assertionToTest != undefined)){
        expression = jsonQuery(assertion[0].expression.toString(), {data: response}).value;
        operator = assertion[0].operator.toLowerCase();
        expectation = assertion[0].expectation.toLowerCase();

        if (operator == "equals"){
            if(expression == expectation){
                success = true;
            }
        }else if(operator == "smallerthan"){
            if(expression < expectation){
                success = true;
            }
        }else if(operator == "doesnotequel"){
            if(expression != expectation){
                success = true;
            }
        }else if(operator == "biggerthan"){
            if(expression > expectation){
                success = true;
            }
        }else {
            success = false;
        }
    }else {
        success = false;
    }
    return success;
}

function extend(target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}