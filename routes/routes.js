/**
 * Created by thomasjansen on 30/03/15.
 */
var AuthEnabled = false;


function ensureAuthenticated(req, res, next) {
    if(AuthEnabled){
        if (req.isAuthenticated())
            return next();
        else {
            res.send({error: "Not logged in"});
        }
    }else{
        return next();
    }
}


module.exports = function(app){
    var requestController = require('../controllers/requestController'),
        accounts = require('../controllers/accounts'),
        scenarios = require('../controllers/scenarios'),
        requests = require('../controllers/tests'),
        assertions = require('../controllers/assertions');

    //REQUEST CALL, controles the outgoing request from the server.
   // app.post('/request', requestController.externalCall);
    //----------------------------------------------------

    //REGISTRATION
    app.get('/register', accounts.getRegister);

    app.post('/register', accounts.registerAccount);
    //----------------------------------------------------

    //LOGIN AND LOGOUT
    app.get('/login', accounts.getLogin);

    app.post('/login', accounts.login);

    app.get('/logout', accounts.logout);

    app.get('/checklogin', accounts.check);
    //----------------------------------------------------

    //TEST PING
    app.get('/ping', accounts.ping);

    app.post('/ping', function(req, res){
        console.log(req);
        res.send("pong");
    });

    app.put('/ping', function(req, res){
        console.log(req);
        res.send("pong");
    });

    app.delete('/ping', function(req, res){
        console.log(req);
        res.send("pong");
    });
    //----------------------------------------------------

    //USERS CRUD
    app.post('/editUser', ensureAuthenticated, accounts.editUser);

    app.get('/user/:_id', ensureAuthenticated, accounts.userDetails);

    app.get('/users', ensureAuthenticated, accounts.users);

    app.get('/user/delete/:_id', ensureAuthenticated, accounts.delete);


    //REQUEST CRUD

    app.get('/requests', ensureAuthenticated, requests.getAll );

    app.post('/request/new', ensureAuthenticated,requests.createTest);

    app.get('/request/:_id', ensureAuthenticated, requests.getTest);

    app.post('/request/update', ensureAuthenticated, requests.updateTest);

    app.get('/request/delete/:_id', ensureAuthenticated, requests.deleteTest);

    app.get('/request/execute/:_id', ensureAuthenticated,requests.executeTest);


    //SCENARIO CRUD

    app.get('/scenarios', ensureAuthenticated, scenarios.getAll);

    app.post('/scenario/new', ensureAuthenticated, scenarios.createScenario);

    app.get('/scenario/:_id', ensureAuthenticated, scenarios.getScenario);

    app.post('/scenario/update', ensureAuthenticated, scenarios.updateScenario);

    app.get('/scenario/delete/:_id', ensureAuthenticated, scenarios.deleteScenario);

    app.get('/scenario/execute/:_id', ensureAuthenticated, scenarios.executeScenario);

    //ASSERTION CRUD

    app.get('/assertions', ensureAuthenticated, assertions.readAllAssertions);

    app.get('/assertion/:_id', ensureAuthenticated, assertions.readAssertion);

    app.post('/assertion/new', ensureAuthenticated, assertions.createAssertion);

    app.post('/assertion/update', ensureAuthenticated, assertions.updateAssertion);

    app.get('/assertion/delete/:_id', ensureAuthenticated, assertions.deleteAssertion);

};



