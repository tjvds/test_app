TESTAPP
===================  
This application allows the user to test websites with individual request and/or scenarios.

COMPONENTS  
==========
All functional back-end code can exists in app.js. Add an http route with app.get(route,action()). 
Within app.js, interface directly with mongodb within the db.init() callback;

Angular html view controllers currently reside within ./public/js.  

USAGE  
=====  
To install:  
npm install  
To run:  
node app.js  
  
  

