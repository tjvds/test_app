function testCtrl($scope,$http){
	$scope.loadMore = function(){
		$http.get('/test').success(function(data){
			console.log(data);
			$scope.vars = data;
		});
	};
}

function refresh($scope, $http, $rootScope){
    $http.get('/checklogin')
        .success(function(data) {
            console.log("Login check " + data);
            $rootScope.loggedIn = data;
            //goodalert(data, $rootScope)
        })
        .error(function(data) {
            alert(data, $rootScope);
        });
}

function homeController($scope, $http, $location, $window, $rootScope){
    loginStateCheck($scope, $http, $rootScope, $location, null);
    $location.path("/");
}

function loginController($scope, $http, $location, $window, $rootScope){
    $location.path("/login");

    $scope.login = function(login){
            $http({
                method: "POST",
                url: "/login",
                data: {username: login.username, password: login.password}
            }).success(function (data) {
                    console.log(data);
                    if (data.succes === 1) {
                        $window.sessionStorage.token = data;
                        console.log("session storage " + $window.sessionStorage);
                        $location.path("/");
                    }
                    else {
                        //console.log(data);
                        console.log("session storage " + $window.sessionStorage);
                        $location.path('/1');
                    }
                }
            ).error(function(data){
                    if (data == "Unauthorized"){
                        alert(data, $rootScope);
                    }
                    console.log(data);
                });
    };
}


function registerController($scope, $http, $location) {
    $location.path("/register");

    $scope.register = function(register){
        console.log("register", register);
        $http({
            method: "POST",
            url: "/register",
            data: {username: register.username, password: register.password, email: register.email}
        }).success(function(data){
            console.log("");
            if (data.succes === 1){
                console.log(data);
                $location.path('/')
            }else{
                console.log(data);
                $location.path('/register')
            }
        }).error(function (data, status, headers, config) {
            alert(data, $rootScope);
        });
    };
}

function logoutController($scope, $http, $location, $window, $rootScope){
    console.log("logoutcontroller");

    $http.get('/logout')
        .success(function(data) {
            console.log("succes \n");
            //loginStateCheck($scope, $http, $rootScope, $location, '/');
            $window.sessionStorage.clear();
            $rootScope.loggedIn = null;
            $location.path("/");
            //$scope.request = data;
        })
        .error(function(data) {
            alert(data, $rootScope);
        });

}

function requestController($scope, $http, $location, $rootScope ,$window, $routeParams){
    //$location.path('/request/:id');
   // loginStateCheck($scope, $http, $rootScope, $location);
    if($routeParams._id !== undefined) {
        $http({
            method: "GET",
            url: "/request/" + $routeParams._id
        }).success(function (data) {
            console.log(data.data[0]);
            $scope.requests = data.data[0];
            $scope.items = data.data[0].parameters
        }).error(function (data) {
            alert(data, $rootScope);
        })
    } else {


    }
}

function loginStateCheck($scope, $http, $rootScope, $location){
    $http.get('/checklogin')
        .success(function(data) {
            console.log("Login check " + data);
            $rootScope.loggedIn = data;
//            if ($redirect){
//                $location.path($redirect);
//            }
        })
        .error(function(data) {
            alert(data, $rootScope);
    });
}


/*
* User Controller
*
* */
function usersController($scope, $http, $location, $window){

    $scope.getAll = function(){
        $http.get('/users')
            .success(function(data) {
                console.log(data.data);
                $scope.users = data.data;
            })
            .error(function(data) {
                alert(data, $rootScope);
            });
    };

    $scope.saveUser = function(){


    };

    $scope.getAll();
}

function userController($scope, $routeParams, $http, $location){
    console.log($routeParams);

    $http.get('/user/' + $routeParams._id)
        .success(function(data) {
            console.log(data.data[0]);
            $scope.user = data.data[0];

        })
        .error(function(data) {
            alert(data, $rootScope);
        });

    $scope.deleteUser = function(id){
        $http.get('/user/delete/' + id)
            .success(function(data){
                console.log("deleted user with id : " + id);
                $location.path("/users")
            }).error(function(data){
                alert(data, $rootScope);
            });
    };

    $scope.save = function(edit){
        $http({
            method: "POST",
            url: "/edituser",
            data: {_id: edit._id, password: edit.password, passwordrepeat: edit.passwordrepeat, email: edit.email}
        })
            .success(function(data){
                console.log("success");
                $location.path('/user/' + edit._id );
            })
            .error(function(data){
                alert(data, $rootScope);
            });

    }
}

function scenariosController($scope, $routeParams, $http, $location, $rootScope){
    console.log("get all scenarios");

    loginStateCheck($scope, $http, $rootScope, $location, null);

    $scope.refresh = function(){
        $http.get('/scenarios')
            .success(function(data){
                console.log(data);
                $scope.scenarios = data.data;
                $scope.result = null;
            })
            .error(function(data){
                alert(data, $rootScope);
            });
    };

    $http.get('/scenarios')
        .success(function(data){
            console.log(data);
            $scope.scenarios = data.data;
        })
        .error(function(data){
            alert(data, $rootScope);
        });

    $scope.run = function(id){
        var scenario;
        var requests = [];
        var result = [];

        $http.get('/scenario/' + id)
            .success(function(data){
                console.log(data);
                scenario = data.data;
                function callback(){
                    console.log(result);
                }

                function thing_with_callback(data, callback) {
                    for(var i = 0; i < data.data[0].requests.length; i++){
                            $http.get('/request/' + data.data[0].requests[i]._id)
                                .success(function (data) {
                                    $http.get('/request/execute/' + data.data[0]._id)
                                        .success(function (data) {
                                            result.push({
                                                result: data
                                            });
                                            goodalert(result, $rootScope);
                                            console.log(result);
                                        }).error(function (data) {
                                            alert(data, $rootScope);
                                        });
                                })
                                .error(function (data) {
                                    alert(data, $rootScope);
                                });

                    }
                }

                thing_with_callback(data, callback);
            })
            .error(function(data){
                alert(data, $rootScope);
            });

    };

    $scope.deleteScenario = function(id){
        console.log("Delete request with id : " + id);

        $http({
            method : "get",
            url: "/scenario/delete/" + id
        }).success(function(data){
            console.log(data);
            $scope.refresh();
            goodalert(data, $rootScope);
        }).error(function(data){
            alert(data, $rootScope);
        })
    };

    $scope.newScenario = function(){
        $location.path('/newscenario');
    };
}

function scenarioController($scope, $routeParams, $http, $window, $rootScope){
    $scope.requests = [];

    $scope.requestsIDs = [];

    $scope.requestsInScenario = [];

    $http.get('/scenario/' + $routeParams._id)
        .success(function(data){
            console.log(data);
            $scope.scenarioData = data.data[0];
            console.log($scope.scenarioData);
            for(var i= 0; i < data.data[0].requests.length; i++){
                console.log(data.data[0].requests[i]._id + i);
                $http.get('/request/' + data.data[0].requests[i]._id)
                    .success(function(data){
                        console.log(data);
                        $scope.requestsInScenario.push({
                            request: data.data[0]
                        });
                    })
                    .error(function(data){
                        alert(data, $rootScope);
                    });

            };

            console.log($scope.requestsInScenario);
            $scope.requestsIDs = data.data[0].requests
        })
        .error(function(data){
            alert(data, $rootScope);
        });

    $http.get('/requests')
        .success(function(data){
            console.log(data);
            $scope.requests = data.data;
            //$scope.result = null;
        })
        .error(function(data){
            alert(data, $rootScope);
        });




    refresh($scope, $http, $rootScope);
}

function newScenarioController($scope, $http, $location, $window, $rootScope){
    //requests that are available
    $scope.requests = [];

    //array for commit
    $scope.requestsIDs = [];

    //requests that will be in the current scenario
    $scope.requestsInScenario = [];

    //get all scenarios
    $http.get('/requests')
        .success(function(data){
            console.log(data);
            $scope.requests = data.data;
            //$scope.result = null;
        })
        .error(function(data){
            alert(data, $rootScope);
        });

    $scope.createScenario = function(scenarioData){
        console.log(scenarioData);
         $http({
            method: "POST",
            url: "/scenario/new",
            data: {name: scenarioData.name, description: scenarioData.description, requests :  $scope.requestsIDs}})
             .success(function(data){
            console.log(data);
        })
            .error(function(data){
                 alert(data, $rootScope);
        });
    };

    $scope.addRequest = function(res){

        console.log(res);
        $scope.requestsInScenario.push({
            request : res
        });

        $scope.requestsIDs.push({
            _id: res._id
        });
    };

    $scope.deleteRequest = function(x){
        console.log(x);
        $scope.requestsInScenario.splice(x,1);
        $scope.requestsIDs.splice(x,1);
    };
    refresh($scope, $http, $rootScope);
}

function requestsController($scope, $http, $location, $window, $rootScope){
    $scope.refresh = function(){
        $http.get('/requests')
            .success(function(data){
                console.log(data);
                $scope.requests = data.data;
                $scope.result = null;
            })
            .error(function(data){
                alert(data, $rootScope);
            });
    };

    $scope.deleteRequest = function(request){
        console.log("Delete request with id : " + request);

        $http({
            method : "get",
            url: "/request/delete/" + request


        }).success(function(data){
            console.log(data);
            $scope.refresh();
        }).error(function(data){
            alert(data, $rootScope);
        })
    };

    $scope.run = function(request){
        console.log("Run request with ID : " + request);
        $http({
            method : "get",
            url: "/request/execute/" + request
        }).success(function(data){
            console.log(data);
            $rootScope.goodalert = null;
            $scope.result = data;
            console.log(data.assertion);
            if(data.assertion == true){
                goodalert(data, $rootScope);
            }else{
                alert(data, $rootScope);
            }

        }).error(function(data){
            alert(data, $rootScope);
        })
    };

    $scope.newRequest = function(){
        $location.path('/newrequest');
    };

    $scope.refresh();
    refresh($scope, $http, $rootScope);
}

function error($scope, $http, $location, $window, $param, $data){
    $location.path('/error');
}

function newRequestController($scope, $http, $rootScope, $window, $location){
    $scope.createRequest = function(requests){
            console.log("create Request", requests);
            $http({
                method: "POST",
                url: "/request/new",
            data : {name: requests.doc.name,
                    description: requests.doc.description,
                    hostname: requests.doc.hostname,
                    port: requests.doc.port,
                    path: requests.doc.path,
                    method: requests.doc.method,
                    parameters: $scope.items,
                    assertion: [{name: requests.doc.assertion.name,
                                expression: requests.doc.assertion.expression,
                                operator: requests.doc.assertion.operator,
                                expectation: requests.doc.assertion.expectation,
                                description: requests.doc.assertion.description
                    }]
        }
            }).success(function(data){
                console.log("");
                if (data.succes === 1){
                    console.log(data);
                    $location.path('/requests')
                }else{
                    console.log(data);
                    //$location.path('/register')
                }
            }).error(function (data, status, headers, config) {
                alert(data, $rootScope);
            });
    };


    $scope.back = function(){
        $location.path('/requests');
    };

    $scope.items = [];
    $scope.addParam = function() {
        $scope.items.push({
            Name: "",
            text: ""
        });
    };

    $scope.deleteParam = function(x){
    $scope.items.splice(x,1);
    };

    refresh($scope, $http, $rootScope);
}

function createAssertionController($scope, $http, $rootScope) {
    $scope.createAssertion = function(assertionData){
            console.log(assertionData);
            $http({
                method: "POST",
                url: "/assertion/new",
                data: {name: assertionData.name,
                       expression : assertionData.expressiondata,
                       operator : assertionData.operator,
                       expectation: assertionData.expectation,
                       description: assertionData.description,
                       idRequest: assertionData.idRequest
                }}).success(function(data){
                    console.log(data);
                })
                .error(function(data){
                    alert(data, $rootScope);
                });
        };
    $scope.findAllRequests = function(){
        //TODO Get all requests name and ID
    }
}

function assertionListController($scope){
    $scope.deleteAssertion = function(id){
            $http.get('/assertion/delete/' + id)
                .success(function(data){
                    console.log("deleted assertion with id : " + id);
                    $location.path("/assertions")
                }).error(function(data){
                    alert(data, $rootScope);
                });
        };

    $scope.getallassertions = function(){
        $http.get('/assertions')
            .success(function(data) {
                console.log("Get all assertions " + data);
                $scope.data = data;
            })
            .error(function(data) {
                alert(data, $rootScope);
            });
    };

    $scope.getOneAssertion = function(id){
        $http.get('/assertion/' + id)
            .success(function(data) {
                console.log("Get "+ id +" assertion " + data);
                $scope.data = data;
            }).error(function(data) {
                alert(data, $rootScope);
            });
    };

    $scope.newAss = function(){
        $location.path('/newassertion');

    };

}

function alert(data, $rootScope){
    $rootScope.goodalert = null;
    $rootScope.alert = data;
}

function goodalert(data, $rootScope){
    $rootScope.goodalert = null;
    $rootScope.goodalert = data;
}


