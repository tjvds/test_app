/**
 * Created by thomasjansen on 30/03/15.
 */
angular.module('testapp', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'views/partials/home.html',
             controller: homeController
        });
        $routeProvider.when('/login', {
            templateUrl: 'views/partials/login.html',
            controller: loginController
        });

        $routeProvider.when('/register', {
            templateUrl: 'views/partials/register.html',
            controller: registerController
        });

        $routeProvider.when('/logout', {
            templateUrl: 'views/partials/logout.html',
            controller: logoutController
        });

        $routeProvider.when('/requests',{
            templateUrl : 'views/partials/requestList.html',
            controller: requestsController
        });

        $routeProvider.when('/request/:_id', {
            templateUrl : 'views/partials/request.html',
            controller: requestController
        });

        $routeProvider.when('/error',{
            templateUrl : 'views/partials/error.html',
            controller: error
        });

        $routeProvider.when('/user/:_id',{
            templateUrl : 'views/partials/user.html',
            controller: userController
        });

        $routeProvider.when('/users',{
            templateUrl : 'views/partials/users.html',
            controller: usersController
        });

        $routeProvider.when('/scenarios',{
            templateUrl : 'views/partials/scenarioList.html',
            controller: scenariosController
        });

        $routeProvider.when('/scenario/:_id',{
            templateUrl : 'views/partials/scenario.html',
            controller: scenarioController
        });

        $routeProvider.when('/newscenario', {
            templateUrl : 'views/partials/newScenario.html',
            controller: newScenarioController
        });

        $routeProvider.when('/newrequest',{
            templateUrl: 'views/partials/newrequest.html',
            controller: newRequestController
        });

        $routeProvider.when('/assertions',{
            templateUrl: 'views/partials/assertionsList.html',
            controller: assertionListController
        });

        $routeProvider.when('/newassertions',{
            templateUrl: 'views/partials/assertions.html',
            controller: assertionListController
        });

        $routeProvider.otherwise({
            redirectTo: "/",
            controller: homeController
        });

    }]);