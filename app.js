// modules used by our app


var express = require('express'),
 index = require('./routes/indexroute');
var http = require('http');
var mongoose = require('mongoose');
var request = require('./models/request.js');
var scenario = require('./models/scenario.js');
var assertion = require('./models/assertion.js');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
var _ = require('underscore');

var app = express();


var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', "GET,PUT,POST,DELETE");
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

app.set('port', process.env.PORT || 3000);
app.set('views', 'public/views');
app.engine('html', require('ejs').renderFile);
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.json());
//app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(passport.initialize());
app.use(passport.session());
app.use(allowCrossDomain);
app.use(app.router);
app.use(express.static('public'));

app.get('/', index.index);

var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

mongoose.connect('mongodb://localhost/leanDB');

require('./routes/routes')(app);

app.listen(3000, function(){
    console.log('Express server listening on port 3000' );
});
