/**
 * Created by thomasjansen on 08/04/15.
 */

var mongoose;
mongoose = require('mongoose'),
    ObjectId = Schema.ObjectId ,
    Schema = mongoose.Schema;


schemaName = new Schema({
    name:{type:String, required:true},
    expression:{type:String, required:true},
    operator:{type:String, required:true},
    expectation:{type:String, required:true},
    description:{type:String, default: ""},
    modificationDate: ({type:Date, default:Date.now})
});

//
//schemaName.path('name').validate(function (val){
//    return(val !== undefined && val !== null && val.length >= 2);
//}, 'invalid request Name');

var modelName = "Assertion";
var collectionName = "assertions";
mongoose.model(modelName, schemaName, collectionName);