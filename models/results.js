/**
 * Created by thomasjansen on 24/04/15.
 */

var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

schemaName = new Schema({
    nameRequest:{type:String, required:true},
    nameAssertion:{type:String, required:true},
    status:{type:String, required:true},
    responseTime:{type:Number, required:true},
    resultAssertion:{type:String, required:true},
    body:{type:String, required:true},
    modificationDate: ({type:Date, default:Date.now})
})

var modelName = "TestResult";
var collectionName = "testResults";
mongoose.model(modelName, schemaName, collectionName);