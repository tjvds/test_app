/**
 * Created by thomasjansen on 02/04/15.
 */
var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var schemaName = Schema({
    name: {type: String, required: true, unique: true},
    requests: [],
    description: {type: String},
    meta: {},
    modificationDate: {type: Date, "default": Date.now}
});

//custom validator
schemaName.path('name').validate(function (val) {
    return (val !== undefined && val !== null && val.length >= 1);
}, 'Invalid name');



var modelName = "Scenario";
var collectionName = "scenarios";
mongoose.model(modelName, schemaName, collectionName);