/**
 * Created by thomasjansen on 30/03/15.
 */
var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schemaName = Schema({
    name: {type: String, required: true, unique: true},
    description: {type: String},
    users: [],
    meta: {},
    modificationDate: {type: Date, "default": Date.now}
});


var modelName = "Group";
var collectionName = "groups";
mongoose.model(modelName, schemaName, collectionName);