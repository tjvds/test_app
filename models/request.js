var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var assertionSchema = new Schema({
    name: {type:String, required:true},
    expression:{type:String, required:true },
    operator:{type:String, required:true },
    expectation:{type:String, required:true },
    description:{type:String, default: "This is an assertion."},
    modificationDate: ({type:Date, default:Date.now})
});

var parameterSchema = Schema({
    name: {type: String, required: true},
    value: {type: String, required: true, default: ""}
});

var resultSchema = Schema({
    Date: {type: Date, default: Date.now},
    result: {type: String, required: true}
});

var schemaName = Schema({
    name: {type: String, required: true, unique: true},
    hostname: {type: String, required: true},
    port: {type: Number, required: true, default: 80},
    path: {type: String, required: true, default :"/"},
    method: {type: String, required: true, default: "GET"},
    headers: [parameterSchema],
    parameters: [parameterSchema],
    description: {type: String},
    assertion: [assertionSchema],
    meta: {},
    modificationDate: {type: Date, "default": Date.now},
    result: [resultSchema]
});


var modelName = "Request";
var collectionName = "requests";
mongoose.model(modelName, schemaName, collectionName);